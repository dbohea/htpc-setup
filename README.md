# HTPC Setup


## Prerequisites

### Install Git

```
sudo apt-get install git
```

### Clone this repo & cd in

```
git clone https://dbohea@bitbucket.org/dbohea/htpc-setup.git && cd htpc-setup
```


## Install one-off deps such as Ansible

```
./init.sh
```


## Run playbook

```
ansible-playbook main.yml -K
```

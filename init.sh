#!/bin/bash

# This file ensures that Ansible and any dependencies are in place.
# You only need to run this file once.
# Setup is designed for & tested against Ubuntu 14 LTS.
# This script will prompt for user input.

# ------------------------------------------------------------------------------

# Install Ansible
# ------------------------------------------------------------------------------

# http://docs.ansible.com/intro_installation.html#latest-releases-via-apt-ubuntu

sudo apt-get -y install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get -y install ansible
